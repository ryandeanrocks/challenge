//
//  Geofence.swift
//  challenge
//
//  Created by Ryan Dean on 4/15/21.
//

import Foundation
import CoreLocation

struct GeoRegion: Codable {
    let latitude: Double?
    let longitude: Double?
    let radius: Double?
    let name: String?
    let notifyOnEnter: Bool?
    let notifyOnExit: Bool?
}

struct GeoEvent: Codable {
    let name: String?
    let isEntry: Bool?
    let timestamp: Double?
}

class Geofence {
    
    static var regions: [GeoRegion] = []
    
    static func loadRegions(){
        
        regions.removeAll()
        
        do {
            regions = try JSONDecoder().decode([GeoRegion].self, from: Database.getRegions()!)
        } catch { print(error) }
        
    }
    
    static func setGeofence() {
        loadRegions()
        
        for r in regions {
            let geofenceRegionCenter: CLLocationCoordinate2D = CLLocationCoordinate2DMake(r.latitude ?? 0.0, r.longitude ?? 0.0)
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter,
                                              radius: r.radius ?? 30,
                                              identifier: r.name ?? "UID")
            
            if r.notifyOnEnter ?? false {
                geofenceRegion.notifyOnEntry = true
            }
            
            if r.notifyOnExit ?? false {
                geofenceRegion.notifyOnExit = true
            }
            
            AppDelegate.locationManager.startMonitoring(for: geofenceRegion)
        }
    }
    
    static func triggerEvent(_ name: String, isEntry: Bool, timestamp: Double) {
        let event = GeoEvent(name: name, isEntry: isEntry, timestamp: timestamp)
        
        print(event)
        var jsonData: Data? = nil
        
        do {
            jsonData = try JSONEncoder().encode(event)
        } catch { print(error) }
        
        if jsonData != nil {
            Database.storeEvent(jsonData!)
        }
    }
}
