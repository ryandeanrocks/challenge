//
//  ViewController.swift
//  challenge
//
//  Created by Ryan Dean on 4/15/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var B_allowLocation: UIButton!
    @IBOutlet weak var B_startGeofence: UIButton!
    @IBOutlet weak var B_showEvents: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func allowLocation(_ sender: Any) {
        // Request User Location
        AppDelegate.locationManager.requestAlwaysAuthorization()
        AppDelegate.locationManager.allowsBackgroundLocationUpdates = true
        
        B_allowLocation.isHidden = true
        B_startGeofence.isHidden = false
    }
    
    @IBAction func startGeofence(_ sender: Any) {
        
        Geofence.setGeofence()
        showAlert("Geofence Monitoring", message: "Monitoring has started, please simulate location with the provided GPX file.")
        
        B_startGeofence.isHidden = true
        B_showEvents.isHidden = false
    }
    
    @IBAction func showEvents(_ sender: Any) {
        Database.printEvents()
        
        var message = ""
        
        for e in Database.events {
            message += "Name: " + e.name!
            if e.isEntry! {
                message += " Event: Entry"
            } else {
                message += " Event: Exit"
            }
            message += " Time: " + String(e.timestamp!) + "\n"
        }
        
        showAlert("Database Events", message: message)
    }
    
    
    
    func didExitArea(_ name: String) {
        showAlert("Exited Area", message: "You justed exited the area " + name)
    }
    
    func didEnterArea(_ name: String) {
        showAlert("Entered Area", message: "You justed entered the area " + name)
    }
    
    func showAlert(_ title: String, message: String) {
        // Create new Alert
        let dialogMessage = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in
         })
        
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    
}

