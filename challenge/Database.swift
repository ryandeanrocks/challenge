//
//  Database.swift
//  challenge
//
//  Created by Ryan Dean on 4/15/21.
//

import Foundation

struct DatabaseRegion: Codable {
    let latitude: Double?
    let longitude: Double?
    let radius: Double?
    let name: String?
    let notifyOnEnter: Bool?
    let notifyOnExit: Bool?
}

struct DatabaseEvent: Codable {
    let name: String?
    let isEntry: Bool?
    let timestamp: Double?
}

class Database {
    static let regions: [DatabaseRegion] = [
        DatabaseRegion(latitude: 37.33233141, longitude: -122.0312186, radius: 100, name: "Apple", notifyOnEnter: true, notifyOnExit: true),
        DatabaseRegion(latitude: 41.8781136, longitude: -87.6297982, radius: 100, name: "Chicago", notifyOnEnter: true, notifyOnExit: true),
        DatabaseRegion(latitude: 32.3668052, longitude: -86.2999689, radius: 100, name: "Montgomery", notifyOnEnter: true, notifyOnExit: true)
    ]
    
    static var events: [DatabaseEvent] = []
    
    static func getRegions() -> Data? {
        var jsonData: Data? = nil
        do {
            jsonData = try JSONEncoder().encode(regions)
        } catch { print(error) }
        return jsonData
    }
    
    static func storeEvent(_ data: Data) {
        var event: DatabaseEvent? = nil
        do {
            event = try JSONDecoder().decode(DatabaseEvent.self, from: data)
        } catch { print(error) }
        if event != nil {
            events.append(event!)
        }
    }
    
    static func printEvents() {
        for e in events {
            print(e)
        }
    }
}
