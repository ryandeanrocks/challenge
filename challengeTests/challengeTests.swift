//
//  challengeTests.swift
//  challengeTests
//
//  Created by Ryan Dean on 4/15/21.
//

import XCTest
@testable import challenge
import CoreLocation

class challengeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRegions() throws {
        Geofence.loadRegions()
        
        for (i, _) in Geofence.regions.enumerated() {
            XCTAssert(Geofence.regions[i].latitude == Database.regions[i].latitude)
            XCTAssert(Geofence.regions[i].longitude == Database.regions[i].longitude)
            XCTAssert(Geofence.regions[i].name == Database.regions[i].name)
            XCTAssert(Geofence.regions[i].radius == Database.regions[i].radius)
            XCTAssert(Geofence.regions[i].notifyOnEnter == Database.regions[i].notifyOnEnter)
            XCTAssert(Geofence.regions[i].notifyOnExit == Database.regions[i].notifyOnExit)
        }
    }
    
    func testEvents() throws {
        let testEvent = GeoEvent(name: "Test", isEntry: false, timestamp: 12345.6)
        
        Geofence.triggerEvent(testEvent.name!, isEntry: testEvent.isEntry!, timestamp: testEvent.timestamp!)
        
        let dbEvent = Database.events.last
        
        XCTAssert(testEvent.name == dbEvent!.name)
        XCTAssert(testEvent.isEntry == dbEvent!.isEntry)
        XCTAssert(testEvent.timestamp == dbEvent!.timestamp)
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
