#  Cohesion Coding Challenge
Repo and project by Corey Dean

## Testing App
1. Open the project in XCode
2. Build the project for an iPhone simulator and launch
3. Authorize location tracking
4. Start geofence monitoring
5. In the iOS simulator, simulate your location using one of the provided GPX files
6. Change your location between the choices to generate event data
7. Press the "Show Database Events" to print the events generated to screen

## Unit Tests
### testRegions()
Ensures that data pulled from the regions table of the database matches the in-memory table and that there are no errors when encoding and decoding
### testEvents()
Creates a sample event and stores it in the database then checks that all data is valid

## Roadmap

1. Create app project
2. Setup location tracking
3. Setup geofencing capability
4. Setup mock database
5. Setup mock API
6. Using API to pull geofencing locations from database
7. Using API to update database with enter and exit data
